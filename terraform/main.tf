provider "aws" {
    profile = "default"
    region  = "us-east-1"

}
provider "aws" {
    profile = "default"
    region  = "us-west-1"
    alias = "USA2"
}


data "aws_ami" "ubuntu" {
     most_recent = true
     filter {
       name   = "name"
       values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
     }
     filter {
       name   = "virtualization-type"
       values = ["hvm"]
    }
    owners = ["099720109477"] # Ubuntu
}


data "aws_ami" "ubuntu_usa2" {
     provider = aws.USA2
     most_recent = true
     filter {
       name   = "name"
       values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
     }
     filter {
       name   = "virtualization-type"
       values = ["hvm"]
    }
    owners = ["099720109477"] # Ubuntu
}


resource "aws_instance" "ubuntu" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.web-server.id]
    key_name = "server2"
    tags = {
      Name = "Ubuntu"
      Owner = "Alexey"
    }
}


resource "aws_instance" "ubuntu_USA2" {
    provider = aws.USA2
    ami = data.aws_ami.ubuntu_usa2.id
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.web-server2.id]
    key_name = "server2"
    tags = {
      Name = "Ubuntu2"
      Owner = "Alexey"
    }
}

#Security Group
resource "aws_security_group" "web-server" {
  name        = "Dynamic SecurityGroup"
  description = "Allow inbound traffic"

dynamic "ingress" {
  for_each = ["80", "443", "22", "8080", "8081", "8082", "9090"]
  content {
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
  Name = "Dynamic SecurityGroup"
  Owner = "Alexey"
  }
}

#Security Group
resource "aws_security_group" "web-server2" {
  provider = aws.USA2
  name        = "Dynamic SecurityGroup"
  description = "Allow inbound traffic"

dynamic "ingress" {
  for_each = ["80", "443", "22", "8080", "8081", "8082", "9090"]
  content {
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
  Name = "Dynamic SecurityGroup"
  Owner = "Alexey"
  }
}
